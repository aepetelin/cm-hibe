package com.lenvo.contactmgr.model;

import javax.persistence.*;

@Entity //or @Entity(name="Contact")
public class Contact {
    @Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int id;
    
		@Column(name="firstName")	
		private String firstName;

    @Column	
    private String lastName;
    
		@Column	
    private String email;
    
		@Column	
    private long phone;
   
		// public constor for JPA
		public Contact() {}

		public Contact(ContactBuilder builder) {
				this.firstName = builder.firstName;
				this.lastName = builder.lastName;
				this.email = builder.email;
				this.phone = builder.phone;
		}

    public Contact(String firstName, String lastName, String email, long phone) {
      this.firstName = firstName;
      this.lastName = lastName;
      this.email = email;
      this.phone = phone;
    }
    
    public int getId() {
      return id;
    }
    public void setId(int id) {
      this.id = id;
    }
    
    public String getFirstName() {
      return firstName;
    }
    public void setFirstName(String firstName) {
      this.firstName = firstName;
    }
    
    public String getLastName() {
      return lastName;
    }
    public void setLastName(String lastName) {
      this.lastName = lastName;
    }
    
    public String getEmail() {
      return email;
    }
    public void setEmail(String email) {
      this.email = email;
    }
    
    public long getPhone() {
      return phone;
    }
    public void setPhone(long phone) {
      this.phone = phone;
    }
    
    public String toString2() {
      return String.format("%s %s (%d)", firstName, lastName, id); 
    }

    @Override
		public String toString() {
			return "Contact{" +
							"id=" + id + 
							", firstname='" + firstName + "\'" +
							", lastname='" + lastName + "\'" +
							", email='" + email + "\'" +
							", phone='" + phone + 
							"}";
		}

		public static class ContactBuilder {
 	 			private String firstName;
    		private String lastName;
    		private String email;
    		private Long phone;
  
				public ContactBuilder(String firstName, String lastName) {
						this.firstName = firstName;
						this.lastName = lastName;
				}

				public ContactBuilder withEmail(String email) {
						// TODO: validate here ...
						this.email = email; 
						return this;
				}

				public ContactBuilder withPhone(Long phone) {
						// TODO: validate here
						this.phone = phone;
						return this;
				}

				public Contact build() {
						return new Contact(this);
				}
		}
}

