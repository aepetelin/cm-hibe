package com.lenvo.contactmgr;

import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.Criteria;

import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import com.lenvo.contactmgr.model.Contact;
import com.lenvo.contactmgr.model.Contact.ContactBuilder;
import java.util.*;
import java.util.stream.Collectors;
import java.io.*;

public class Application {

	private static final SessionFactory sessionFactory = buildSessionFactory(); 

	private static SessionFactory buildSessionFactory() {
		final ServiceRegistry registry = new StandardServiceRegistryBuilder()
			.configure()
			.build();

			return new MetadataSources(registry)
				.buildMetadata()
				.buildSessionFactory();
	} 

	public static void main(String[] args) {
		Contact contact = new ContactBuilder("Jimmy", "McGill")
			.withEmail("gimmymcgill@gmail.com")
			.withPhone(7778001112L)
			.build();

		System.out.printf('\n' + contact.toString() + '\n' + '\n');
		save(contact);
	
		contact = new ContactBuilder("Howard", "Jackson")
			.withEmail("gimmymcgill@gmail.com")
			.withPhone(7778001112L)
			.build(); 
		save(contact);
		
		contact = new ContactBuilder("Tuco", "Salamanca")
			.withEmail("gimmymcgill@gmail.com")
			.withPhone(7778001112L)
			.build(); 
		save(contact);
		
		contact = new ContactBuilder("Chuck", "McGill")
			.withEmail("gimmymcgill@gmail.com")
			.withPhone(900L)
			.build();	
		int id = save(contact);

		//fetchAllContacts().stream()
		//	.collect(Collectors.toList())
		//	.forEach(c -> System.out.printf('\n' + c.toString() + '\n'));
		fetchAllContacts().stream()
			.forEach(System.out::println);

		System.out.printf("updating ... %n%n");
		Contact contUpd = findContactById(id);
		contUpd.setPhone(contUpd.getPhone() + fetchAllContacts().size());
		update(contUpd);

		List<Contact> tmp = fetchAllContacts();
 
		int firstId = tmp.get(0).getId();
		int secondId = tmp.get(1).getId();
		deleteById(firstId);
		deleteById(secondId);

		fetchAllContacts().stream()
			.forEach(System.out::println);
	}                              	
                                   	
	private static int save(Contact contact) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		int id = (int)session.save(contact);
		session.getTransaction().commit();
		session.close();
		return id;
	}

	private static Contact findContactById(int id) {
		Session session = sessionFactory.openSession();
		Contact contact = session.get(Contact.class, id);
		session.close();
		return contact;
	}

	private static void update(Contact contact) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.update(contact);
		session.getTransaction().commit();
		session.close();		
	}

	private static void deleteById(int id) {
		Session session = sessionFactory.openSession();
		Contact contact = session.get(Contact.class, id);
		session.beginTransaction();
		session.delete(contact);
		session.getTransaction().commit();
		session.close();
	}

	@SuppressWarnings("unchecked")
	private static List<Contact> fetchAllContacts() {
		Session session = sessionFactory.openSession();
		
		Criteria criteria = session.createCriteria(Contact.class);
		List<Contact> contacts = criteria.list();

		session.close();

		return contacts;
	}

}
